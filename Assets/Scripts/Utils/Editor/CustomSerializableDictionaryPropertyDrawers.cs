﻿using UnityEditor;

namespace MergeTestGame
{
    [CustomPropertyDrawer(typeof(StringCreatureViewDictionary))]
    [CustomPropertyDrawer(typeof(StringCreatureModelDictionary))]
    [CustomPropertyDrawer(typeof(StringCreatureIdCollectableItemDictionary))]
    public class AnySerializableDictionaryPropertyDrawer : SerializableDictionaryPropertyDrawer { }
}