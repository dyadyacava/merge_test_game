﻿using System;

namespace MergeTestGame
{
    [Serializable]
    public class StringCreatureViewDictionary : SerializableDictionary<string, CreatureView> { }

    [Serializable]
    public class StringCreatureModelDictionary : SerializableDictionary<string, CreatureModel> { }

    [Serializable]
    public class StringCreatureIdCollectableItemDictionary : SerializableDictionary<string, CollectableItem> { }
}