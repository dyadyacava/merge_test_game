using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace MergeTestGame
{
    [CreateAssetMenu(menuName = "MergeTestGame/Settings/PrefabSettings")]
    public class PrefabSettings : ScriptableObject
    {
        [SerializeField] private StringCreatureViewDictionary _creatureViewsPrefabs;
        [SerializeField] private StringCreatureIdCollectableItemDictionary _collectableItemPrefabs;

        public IEnumerable<CreatureView> CreatureViewPrefabs => _creatureViewsPrefabs.Values.ToList().Distinct();

        public IEnumerable<CollectableItem> CollectableItemPrefabs => _collectableItemPrefabs.Values.ToList().Distinct();

        public CreatureView GetCreatureView(string id)
        {
            _creatureViewsPrefabs.TryGetValue(id, out var view);
            return view;
        }

        public CollectableItem GetCollectableItem(string id)
        {
            _collectableItemPrefabs.TryGetValue(id, out var item);
            return item;
        }
    }
}