using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CreatureState
{
    Locked = 0,
    Unlocked = 1
}
