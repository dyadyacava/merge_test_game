using SimpleMan.Extensions;
using System;
using UnityEngine;
using Zenject;

namespace MergeTestGame
{
    public class Creature : MonoBehaviour, 
        IPoolable<string, CreatureModel, CreatureView, IMemoryPool>, IDisposable
    {
        [Inject]
        private MergeSystem _mergeSystem;

        [Inject]
        private LocationSystem _locationSystem;

        [Inject]
        private CreatureViewFactory _creatureViewFactory;

        private string _id;
        private int _level = 0;
        private CreatureModel _model;
        private CreatureState _state = CreatureState.Locked;
        private CreatureView _view;
        private Coroutine _spawnRoutine = null;
        private bool _wasUnlocked = false;

        IMemoryPool _pool;

        public string Id => _id;

        public int Level
        {
            get => _level;
            set
            {
                _level = value;
                View?.UpdateCreatureLevel(_level);
            }
        }

        public CreatureModel Model => _model;

        public CreatureView View
        {
            get => _view;
            set
            {
                if (_view == value)
                {
                    return;
                }
                else
                {
                    if (_view != null)
                    {
                        _view.StartDrag -= OnStartDrag;
                        _view.EndDrag -= OnEndDrag;
                        _view.Click -= OnClick;
                    }                    

                    _view = value;

                    if (_view != null)
                    {
                        _view.UpdateCreatureState(State);
                        _view.UpdateCreatureLevel(Level);

                        _view.StartDrag += OnStartDrag;
                        _view.EndDrag += OnEndDrag;
                        _view.Click += OnClick;
                    }
                }
            }
        }

        public CreatureState State
        {
            get => _state;
            set
            {
                if (_state != value)
                {
                    _state = value;
                    _view?.UpdateCreatureState(_state);
                }
            }
        }

        public event Action<Creature> Destroyed;

        protected void Update()
        {
            if (_spawnRoutine == null && CanSpawnCollectableItems())
            {
                _spawnRoutine = this.RepeatUntil(CanSpawnCollectableItems, 
                    () => SpawnCollectableItem(true), OnFillLocation, _model.SpawnRewardInterval);
            }
        }

        public void Init(string id, CreatureModel model, CreatureView viewPrefab)
        {
            _id = id;
            _model = model;
            Level = _locationSystem.GetCreatureLevelInHierarchy(_id);

            CreatureView oldView = View;
            Vector3? oldPos = oldView?.transform.position ?? null;
            if (oldView != null)
            {
                oldView.Dispose();
                View = null;
            }

            int prefabId = viewPrefab.GetInstanceID();
            var view = _creatureViewFactory.Create(prefabId, oldPos);
            View = view;
        }

        public void Unlock()
        {
            State = CreatureState.Unlocked;
        }

        public void CheckMerge()
        {
            _mergeSystem.TryMergeCreature(this);
        }

        public void Destroy()
        {
            CreatureView view = View;
            view.Dispose();
            View = null;
            Destroyed?.Invoke(this);
        }

        private void OnStartDrag()
        {
            if (State == CreatureState.Locked)
            {
                Unlock();
                _wasUnlocked = true;
            }
        }

        private void OnEndDrag()
        {
            CheckMerge();
        }

        private void OnClick()
        {
            if (State == CreatureState.Unlocked && !_wasUnlocked)
            {
                SpawnCollectableItem(false);
            }
            _wasUnlocked = false;
        }

        private bool CanSpawnCollectableItems()
        {
            return State == CreatureState.Unlocked;
        }

        private void SpawnCollectableItem(bool isIdle = true)
        {
            _locationSystem.SpawnCollectableItem(this, isIdle);
        }

        private void OnFillLocation()
        {
            _spawnRoutine = null;
        }

        public void OnDespawned()
        {
            _pool = null;
        }

        public void OnSpawned(string id, CreatureModel model, CreatureView viewPrefab, IMemoryPool pool)
        {
            _pool = pool;
            State = CreatureState.Locked;
            Init(id, model, viewPrefab);
        }

        public void Dispose()
        {
            _pool.Despawn(this);

            StopCoroutine(_spawnRoutine);
            _spawnRoutine = null;
        }

        public class Factory : PlaceholderFactory<string, CreatureModel, CreatureView, Creature>
        {
        }
    }
}