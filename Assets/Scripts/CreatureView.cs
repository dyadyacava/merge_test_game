using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Zenject;

namespace MergeTestGame
{
    public class CreatureView : MonoBehaviour, IPoolable<Vector3?, IMemoryPool>, IDisposable,
        IDragHandler, IEndDragHandler, IPointerDownHandler, IPointerUpHandler
    {
        [SerializeField] private GameObject _lockedView;
        [SerializeField] private GameObject _unlockedView;
        [SerializeField] private Text _levelLabel;

        [Inject]
        private LocationSystem _locationSystem;

        private RectTransform _rect;
        private bool _isInDrag = false;
        private IMemoryPool _pool;

        private RectTransform _gameZone => _locationSystem.GameZoneRect;

        public RectTransform RectTransform => _rect;

        public event Action StartDrag, EndDrag, Click;

        private void Awake()
        {
            _rect = GetComponent<RectTransform>();
        }

        private void PlaceCreatureOnGrid(RectTransform gameZone)
        {
            Rect rect = gameZone.rect;
            var pos = gameZone.transform.position + new Vector3(UnityEngine.Random.Range(rect.xMin, rect.xMax),
                    UnityEngine.Random.Range(rect.yMin, rect.yMax), 0);
            transform.position = pos;
        }

        public void UpdateCreatureState(CreatureState state)
        {
            switch (state)
            {
                case CreatureState.Locked:
                    _lockedView.SetActive(true);
                    _unlockedView.SetActive(false);
                    break;
                case CreatureState.Unlocked:
                    _lockedView.SetActive(false);
                    _unlockedView.SetActive(true);
                    break;
            }
        }

        public void UpdateCreatureLevel(int level)
        {
            _levelLabel.text = $"{level}";
        }

        public void OnDrag(PointerEventData eventData)
        {
            if (_isInDrag)
            {
                Vector3 newPos = transform.position + (Vector3)eventData.delta;
                bool isContains = RectTransformUtility.RectangleContainsScreenPoint(_gameZone, newPos);
                if (isContains)
                {
                    transform.position = newPos;
                }
            }
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            _isInDrag = false;
            EndDrag?.Invoke();
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            _isInDrag = true;
            StartDrag?.Invoke();
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            Click?.Invoke();
        }

        public void Dispose()
        {
            _pool.Despawn(this);
        }

        public void OnSpawned(Vector3? pos, IMemoryPool pool)
        {
            _pool = pool;

            transform.SetParent(_gameZone.transform);

            if (pos.HasValue)
            {
                transform.position = pos.Value;
            }
            else
            {
                PlaceCreatureOnGrid(_gameZone);
            }
        }

        public void OnDespawned()
        {
            _pool = null;
        }

        public class Factory : PlaceholderFactory<Vector3?, CreatureView>
        {
        }
    }
}