using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Zenject;

namespace MergeTestGame
{
    public class CollectableItem : MonoBehaviour, 
        IPoolable<string, int, bool, IMemoryPool>, IDisposable, IPointerDownHandler
    {
        [SerializeField] private Text _amountLabel;

        [Inject]
        private ModelsMapping _modelsMapping;

        private Reward _reward;
        private string _creatureId;
        private int _creatureLevel;
        private bool _isIdle;

        private IMemoryPool _pool;

        public Reward Reward => _reward;

        public string CreatureId => _creatureId;
        public int CreatureLevel => _creatureLevel;
        public bool IsIdle => _isIdle;

        public event Action<CollectableItem> Collect;
        public event Action<CollectableItem> Destroyed;

        private void Init(string creatureId, int level, bool isIdle)
        {
            _creatureId = creatureId;
            _creatureLevel = level;
            _isIdle = isIdle;

            CreatureModel creatureModel = _modelsMapping.GetCreatureModel(creatureId);
            int modifier = isIdle ? creatureModel.IdleRewardAmountModifier : creatureModel.TapRewardAmountModifier;
            int amount = ((int)Math.Pow(2, level - 1)) * modifier;
            CurrencyType currencyType = creatureModel.CollectableItemType;
            _reward = new Reward(currencyType, amount);
            _amountLabel.text = $"{amount}";
        }

        public void Dispose()
        {
            _pool.Despawn(this);
        }

        public void OnSpawned(string creatureId, int level, bool isIdle, IMemoryPool pool)
        {
            _pool = pool;
            Init(creatureId, level, isIdle);
        }

        public void OnDespawned()
        {
            _pool = null;
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            Collect?.Invoke(this);
        }

        public class Factory : PlaceholderFactory<string, int, bool, CollectableItem>
        {
        }
    }
}