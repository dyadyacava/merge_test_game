using System;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace MergeTestGame
{
    public class CurrencyWidget : MonoBehaviour, IInitializable, IDisposable
    {
        [SerializeField] private Image _icon;
        [SerializeField] private Text _counter;
        [SerializeField] private CurrencyType _currencyType;

        [Inject]
        private CurrencySystem _inventorySystem;

        [Inject]
        public void Initialize()
        {
            _counter.text = $"{0}";
            _inventorySystem.CurrencyAmountChanged += OnCurrencyAmountChanged;
        }

        public void Dispose()
        {
            _inventorySystem.CurrencyAmountChanged -= OnCurrencyAmountChanged;
        }

        private void OnCurrencyAmountChanged(CurrencyType type, int amount)
        {
            if (_currencyType == type)
            {
                _counter.text = $"{amount}";
            }
        }
    }
}