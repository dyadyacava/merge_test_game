namespace MergeTestGame
{
    public class Reward
    {
        private CurrencyType _currencyType;
        private int _amount;

        public CurrencyType CurrencyType => _currencyType;
        public int Amount => _amount;

        public Reward(CurrencyType currencyType, int amount)
        {
            _currencyType = currencyType;
            _amount = amount;
        }
    }
}