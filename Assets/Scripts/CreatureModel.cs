using System;
using UnityEngine;

namespace MergeTestGame
{
    [CreateAssetMenu(menuName = "MergeTestGame/Models/CreatureModel")]
    [Serializable]
    public class CreatureModel : ScriptableObject
    {
        public float SpawnRewardInterval;
        public int IdleRewardAmountModifier = 1;
        public int TapRewardAmountModifier = 1;
        public CurrencyType CollectableItemType = CurrencyType.Soft;
    }
}