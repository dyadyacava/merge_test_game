using UnityEngine;
using Zenject;

namespace MergeTestGame
{
    public class CollectableItemsSystem : MonoBehaviour
    {
        [SerializeField] private Transform _collectableItemsContainer;
        [SerializeField] private float _minSpawnRadius = 50;
        [SerializeField] private float _maxSpawnRadius = 200;

        [Inject]
        private PrefabSettings _prefabSettings;

        [Inject]
        private LocationSystem _locationSystem;

        [Inject]
        private readonly CollectableItemFactory _collectableItemFactory;

        public CollectableItem CreateCollectableItem(Creature creature, bool isIdle)
        {
            CollectableItem collectableItem = CreateCollectableItem(creature.Id, creature.Level, isIdle);

            //@TODO add check gameZone rect

            int rand = Random.Range(0, 2);
            float dx = rand == 0 ? Random.Range(-_maxSpawnRadius, -_minSpawnRadius) : Random.Range(_minSpawnRadius, _maxSpawnRadius);
            rand = Random.Range(0, 1);
            float dy = rand == 0 ? Random.Range(-_maxSpawnRadius, -_minSpawnRadius) : Random.Range(_minSpawnRadius, _maxSpawnRadius);

            var pos = creature.View.transform.position + new Vector3(dx, dy, 0);

            collectableItem.transform.position = pos;

            return collectableItem;
        }

        public CollectableItem CreateCollectableItem(string creatureId, int creatureLevel, bool isIdle, Vector3 pos)
        {
            CollectableItem collectableItem = CreateCollectableItem(creatureId, creatureLevel, isIdle);

            collectableItem.transform.position = pos;

            return collectableItem;
        }

        private CollectableItem CreateCollectableItem(string creatureId, int creatureLevel, bool isIdle)
        {
            CollectableItem itemPrefab = _prefabSettings.GetCollectableItem(creatureId);

            int prefabId = itemPrefab.GetInstanceID();
            var collectableItem = _collectableItemFactory.Create(prefabId, creatureId, creatureLevel, isIdle);
            collectableItem.transform.SetParent(_collectableItemsContainer);

            return collectableItem;
        }
    }
}