using UnityEngine;
using Zenject;

namespace MergeTestGame
{
    public class CreaturesSystem : MonoBehaviour
    {
        [SerializeField] private Creature _creaturePrefab;
        [SerializeField] private Transform _creaturesContainer;

        [Inject]
        private ModelsMapping _modelsMapping;

        [Inject]
        private PrefabSettings _prefabSettings;

        [Inject]
        private LocationSystem _locationSystem;

        [Inject]
        private MergeSystem _mergeSystem;

        [Inject]
        private readonly Creature.Factory _creatureFactory;

        public Creature CreateCreature(string id)
        {
            var model = _modelsMapping.GetCreatureModel(id);
            var viewPrefab = _prefabSettings.GetCreatureView(id);

            var creature = _creatureFactory.Create(id, model, viewPrefab);
            creature.transform.SetParent(_creaturesContainer);

            return creature;
        }

        public bool TryUpgradeCreature(Creature creature)
        {
            string nextId = _locationSystem.GetNextCreatureIdInHierarchy(creature.Id);

            if (!nextId.Equals(string.Empty))
            {
                var model = _modelsMapping.GetCreatureModel(nextId);
                var viewPrefab = _prefabSettings.GetCreatureView(nextId);
                creature.Init(nextId, model, viewPrefab);
                return true;
            }

            return false;
        }
    }
}