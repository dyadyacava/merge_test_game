using System.Linq;
using UnityEngine;
using Zenject;

namespace MergeTestGame
{
    public class MergeSystem : MonoBehaviour
    {
        [Inject]
        private LocationSystem _locationSystem;

        [Inject]
        private CreaturesSystem _creaturesFactory;

        public void TryMergeCreature(Creature creature)
        {
            RectTransform rt = creature.View.RectTransform;
            Creature creatureToMerge = _locationSystem.Creatures.FirstOrDefault(cr =>
            {
                bool isSame = cr != creature && cr.State == CreatureState.Unlocked && cr.Id.Equals(creature.Id);
                RectTransform rt2 = cr.View.RectTransform;
                bool isOverlaped = RectUtils.RectOverlaps(rt, rt2);
                return isSame && isOverlaped;
            });

            if (creatureToMerge != null)
            {
                if (_creaturesFactory.TryUpgradeCreature(creature))
                {
                    // Merge
                    creatureToMerge.Destroy();
                }
            }
        }
    }
}