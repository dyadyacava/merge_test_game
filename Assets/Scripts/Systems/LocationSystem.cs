using SimpleMan.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Zenject;

namespace MergeTestGame
{
    public class LocationSystem : MonoBehaviour, IInitializable
    {
        [SerializeField] private int _maxCreaturesAmount = 3;
        [SerializeField] private float _delayBetweenSpawn = 1f;

        // @TODO replace to economics
        [SerializeField] private List<string> _creatureHierarchy;
        [SerializeField] private RectTransform _gameZoneRect;

        [Inject]
        private CreaturesSystem _creaturesSystem;

        [Inject]
        private CollectableItemsSystem _collectableItemsSystem;

        private Coroutine _spawnRoutine = null;
        private bool _isInitialized = false;

        private List<Creature> _creatures = new List<Creature>();
        private List<CollectableItem> _collectableItems = new List<CollectableItem>();

        private string _primaryCreature => _creatureHierarchy.FirstOrDefault();

        public RectTransform GameZoneRect => _gameZoneRect;

        public IReadOnlyList<Creature> Creatures => _creatures;

        public event Action<CollectableItem> CollectableItemCreated;
        public event Action<CollectableItem> CollectableItemDestroyed;

        protected void Update()
        {
            if (_isInitialized && _spawnRoutine == null && IsLocationNotFull())
            {
                _spawnRoutine = this.RepeatUntil(IsLocationNotFull, 
                    () => SpawnCreature(_primaryCreature), 
                    OnFillLocation, 
                    _delayBetweenSpawn);
            }
        }

        public void OnDestroy()
        {
            StopCoroutine(_spawnRoutine);
            _spawnRoutine = null;
        }

        public void SpawnCollectableItem(Creature creature, bool isIdle)
        {
            CollectableItem item = _collectableItemsSystem.CreateCollectableItem(creature, isIdle);
            item.Destroyed += OnCollectableItemDestroyed;
            _collectableItems.Add(item);
            SaveCollectableItems();
            CollectableItemCreated?.Invoke(item);
        }

        private void SpawnCollectableItem(string creatureId, int creatureLevel, bool isIdle, Vector3 pos, bool needSave = true)
        {
            CollectableItem item = _collectableItemsSystem.CreateCollectableItem(creatureId, creatureLevel, isIdle, pos);
            item.Destroyed += OnCollectableItemDestroyed;
            _collectableItems.Add(item);
            CollectableItemCreated?.Invoke(item);

            if (needSave)
            {
                SaveCollectableItems();
            }
        }

        public string GetNextCreatureIdInHierarchy(string id)
        {
            int index = _creatureHierarchy.IndexOf(id);
            if (index > -1 && index + 1 < _creatureHierarchy.Count)
            {
                return _creatureHierarchy[index + 1];
            }
            return string.Empty;
        }

        public int GetCreatureLevelInHierarchy(string id)
        {
            int index = _creatureHierarchy.IndexOf(id);
            return index + 1;
        }

        public string GetCreatureIdByLevelInHierarchy(int level)
        {
            int id = level - 1;
            if (id > -1 && id < _creatureHierarchy.Count)
            {
                return _creatureHierarchy[id];
            }
            return null;
        }

        private bool IsLocationNotFull()
        {
            return _creatures.Count < _maxCreaturesAmount;
        }

        private Creature SpawnCreature(string id, bool needSave = true)
        {
            Creature creature = _creaturesSystem.CreateCreature(id);
            creature.Destroyed += OnCreatureDestroyed;
            _creatures.Add(creature);

            if (needSave)
            {
                SaveCreatures();
            }

            return creature;
        }

        private void OnFillLocation()
        {
            _spawnRoutine = null;
        }

        private void OnCreatureDestroyed(Creature creature)
        {
            creature.Destroyed -= OnCreatureDestroyed;
            if (_creatures.IndexOf(creature) != -1)
            {
                creature.Dispose();
                _creatures.Remove(creature);
                SaveCreatures();
            }
        }

        private void OnCollectableItemDestroyed(CollectableItem item)
        {
            item.Destroyed -= OnCollectableItemDestroyed;
            if (_collectableItems.IndexOf(item) != -1)
            {
                _collectableItems.Remove(item);
                SaveCollectableItems();
            }
            CollectableItemDestroyed?.Invoke(item);
        }

        private void SaveCreatures(bool needSave = true)
        {
            int creaturesAmount = _creatures.Count;
            PlayerPrefs.SetInt("CreaturesAmount", creaturesAmount);
            
            for (int i = 0; i < creaturesAmount; i ++)
            {
                Creature creature = _creatures[i];
                Vector3 pos = creature.View.transform.position;
                PlayerPrefs.SetFloat($"Creature{i}.x", pos.x);
                PlayerPrefs.SetFloat($"Creature{i}.y", pos.y);
                PlayerPrefs.SetInt($"Creature{i}.state", (int)creature.State);
                PlayerPrefs.SetInt($"Creature{i}.level", creature.Level);
            }

            if (needSave)
            {
                PlayerPrefs.Save();
            }
        }

        private void SaveCollectableItems(bool needSave = true)
        {
            int collectableItemsAmount = _collectableItems.Count;
            PlayerPrefs.SetInt("CollectableItemsAmount", collectableItemsAmount);

            for (int i = 0; i < collectableItemsAmount; i++)
            {
                CollectableItem collectableItem = _collectableItems[i];
                Vector3 pos = collectableItem.transform.position;
                PlayerPrefs.SetFloat($"CollectableItem{i}.x", pos.x);
                PlayerPrefs.SetFloat($"CollectableItem{i}.y", pos.y);
                PlayerPrefs.SetString($"CollectableItem{i}.creatureId", collectableItem.CreatureId);
                PlayerPrefs.SetInt($"CollectableItem{i}.creatureLevel", collectableItem.CreatureLevel);
                PlayerPrefs.SetInt($"CollectableItem{i}.isIdle", collectableItem.IsIdle ? 1 : 0);
            }

            if (needSave)
            {
                PlayerPrefs.Save();
            }
        }

        private void Save()
        {
            SaveCreatures(false);
            SaveCollectableItems();
        }

        private void LoadCreatures()
        {
            int creaturesAmount = PlayerPrefs.GetInt("CreaturesAmount");
            for (int i = 0; i < creaturesAmount; i++)
            {
                Vector3 pos = new Vector3();
                pos.x = PlayerPrefs.GetFloat($"Creature{i}.x");
                pos.y = PlayerPrefs.GetFloat($"Creature{i}.y");
                CreatureState state = (CreatureState)PlayerPrefs.GetInt($"Creature{i}.state");
                int level = PlayerPrefs.GetInt($"Creature{i}.level");
                string id = GetCreatureIdByLevelInHierarchy(level);
                Creature creature = SpawnCreature(id, false);
                creature.View.transform.position = pos;

                if (state == CreatureState.Unlocked)
                {
                    creature.Unlock();
                }
            }
        }

        private void LoadCollectableItems()
        {
            int collectableItemsAmount = PlayerPrefs.GetInt("CollectableItemsAmount");
            for (int i = 0; i < collectableItemsAmount; i++)
            {
                Vector3 pos = new Vector3();
                pos.x = PlayerPrefs.GetFloat($"CollectableItem{i}.x");
                pos.y = PlayerPrefs.GetFloat($"CollectableItem{i}.y");
                string creatureId = PlayerPrefs.GetString($"CollectableItem{i}.creatureId");
                int creatureLevel = PlayerPrefs.GetInt($"CollectableItem{i}.creatureLevel");
                bool isIdle = PlayerPrefs.GetInt($"CollectableItem{i}.isIdle") == 1;
                SpawnCollectableItem(creatureId, creatureLevel, isIdle, pos, false);
            }
        }

        private void Load()
        {
            LoadCreatures();
            LoadCollectableItems();
        }

        [Inject]
        public void Initialize()
        {
            _isInitialized = true;
            Load();
        }
    }
}