using System;
using UnityEngine;
using Zenject;

namespace MergeTestGame
{
    public class CollectSystem : MonoBehaviour, IInitializable, IDisposable
    {
        [Inject]
        private LocationSystem _locationSystem;

        [Inject]
        private CurrencySystem _currencySystem;

        [Inject]
        public void Initialize()
        {
            _locationSystem.CollectableItemCreated += OnCollectableItemCreated;
        }

        public void Dispose()
        {
            _locationSystem.CollectableItemCreated -= OnCollectableItemCreated;
        }

        private void OnCollectableItemCreated(CollectableItem item)
        {
            item.Collect += OnItemCollected;
        }

        private void OnItemCollected(CollectableItem item)
        {
            item.Collect -= OnItemCollected;

            _currencySystem.ApplyReward(item.Reward);

            item.Dispose();
        }
    }
}