using System;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace MergeTestGame
{
    public class CurrencySystem : MonoBehaviour, IInitializable
    {
        //@TODO implement saving
        private Dictionary<CurrencyType, int> _currencyAmounts = new Dictionary<CurrencyType, int>();
        
        private Action<CurrencyType, int> _myEvent;
        public event Action<CurrencyType, int> CurrencyAmountChanged
        {
            add
            {
                lock (this)
                {
                    _myEvent += value;
                    foreach (var pair in _currencyAmounts)
                    {
                        value.Invoke(pair.Key, pair.Value);
                    }
                }
            }
            remove
            {
                lock (this)
                {
                    _myEvent -= value;
                }
            }
        }

        [Inject]
        public void Initialize()
        {
            _currencyAmounts.Add(CurrencyType.Soft, 0);
            _currencyAmounts.Add(CurrencyType.Hard, 0);

            Load();
        }

        public void AddCurrency(CurrencyType type, int amount)
        {
            _currencyAmounts[type] += amount;
            _myEvent?.Invoke(type, _currencyAmounts[type]);
            Save(type);
        }

        public void ApplyReward(Reward reward)
        {
            AddCurrency(reward.CurrencyType, reward.Amount);
        }

        public int GetCurrencyAmount(CurrencyType type)
        {
            int amount = 0;
            _currencyAmounts.TryGetValue(type, out amount);
            return amount;
        }

        private void Save(CurrencyType type)
        {
            PlayerPrefs.SetInt($"Currency_{type.ToString()}", GetCurrencyAmount(type));
        }

        private void Load()
        {
            string[] types = Enum.GetNames(typeof(CurrencyType));
            foreach (string typeName in types)
            {
                int amount = 0;
                string key = $"Currency_{typeName.ToString()}";
                if (PlayerPrefs.HasKey(key))
                {
                    amount = PlayerPrefs.GetInt(key);
                }
                CurrencyType type = (CurrencyType)Enum.Parse(typeof(CurrencyType), typeName);
                _currencyAmounts[type] = amount;
                _myEvent?.Invoke(type, amount);
            }
        }
    }
}