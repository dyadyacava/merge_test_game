using UnityEngine;

namespace MergeTestGame
{
    [CreateAssetMenu(menuName = "MergeTestGame/Settings/ModelsMapping")]
    public class ModelsMapping : ScriptableObject
    {
        [SerializeField] private StringCreatureModelDictionary _creatureModels;

        public CreatureModel GetCreatureModel(string id)
        {
            _creatureModels.TryGetValue(id, out var model);
            return model;
        }
    }
}