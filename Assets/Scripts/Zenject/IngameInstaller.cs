using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Zenject;

namespace MergeTestGame
{
    public class IngameInstaller : MonoInstaller<IngameInstaller>
    {
        [SerializeField] private ModelsMapping _modelsMapping;
        [SerializeField] private PrefabSettings _prefabSettings;

        [SerializeField] private Creature _creaturePrefab;
        [SerializeField] private CollectableItem _collectableItemPrefab;

        public override void InstallBindings()
        {
            // Systems

            Container.Bind<MergeSystem>().FromComponentsInHierarchy().AsSingle();
            Container.Bind<CurrencySystem>().FromComponentsInHierarchy().AsSingle();
            Container.Bind<CreaturesSystem>().FromComponentsInHierarchy().AsSingle();
            Container.Bind<CollectableItemsSystem>().FromComponentsInHierarchy().AsSingle();
            Container.Bind<CollectSystem>().FromComponentsInHierarchy().AsSingle();
            Container.Bind<LocationSystem>().FromComponentsInHierarchy().AsSingle();

            // Settings

            Container.BindInstance(_modelsMapping);
            Container.BindInstance(_prefabSettings);

            // Creatures

            Container.BindFactory<string, CreatureModel, CreatureView, Creature, Creature.Factory>().
                FromMonoPoolableMemoryPool(x => x.WithInitialSize(20).
                    FromComponentInNewPrefab(_creaturePrefab).
                    UnderTransformGroup("CreaturesPool"));

            // Creature Views

            foreach (CreatureView viewPrefab in _prefabSettings.CreatureViewPrefabs)
            {
                int id = viewPrefab.GetInstanceID();
                string name = viewPrefab.name;

                Container.BindFactory<Vector3?, CreatureView, CreatureView.Factory>().
                    WithId(id).
                    FromMonoPoolableMemoryPool(x => x.WithInitialSize(10).
                    FromComponentInNewPrefab(viewPrefab).
                    UnderTransformGroup($"{name}_{id}_Pool"));
            }

            Container.Bind<Dictionary<int, IFactory<Vector3?, CreatureView>>>()
                .FromMethod(GetCreatureViewFactories).WhenInjectedInto<CreatureViewFactory>();

            Container.Bind<CreatureViewFactory>().AsSingle();

            // Collectible Items

            foreach (CollectableItem itemPrefab in _prefabSettings.CollectableItemPrefabs)
            {
                int id = itemPrefab.GetInstanceID();
                string name = itemPrefab.name;

                Container.BindFactory<string, int, bool, CollectableItem, CollectableItem.Factory>().
                    WithId(id).
                    FromMonoPoolableMemoryPool(x => x.WithInitialSize(10).
                    FromComponentInNewPrefab(itemPrefab).
                    UnderTransformGroup($"{name}_{id}_Pool"));
            }

            Container.Bind<Dictionary<int, IFactory<string, int, bool, CollectableItem>>>()
                .FromMethod(GetCollectableItemFactories).WhenInjectedInto<CollectableItemFactory>();

            Container.Bind<CollectableItemFactory>().AsSingle();
        }

        private Dictionary<int, IFactory<Vector3?, CreatureView>> GetCreatureViewFactories(InjectContext ctx)
        {
            return ctx.Container.AllContracts.Where(
                x => x.Type == typeof(CreatureView.Factory))
                .ToDictionary(x => (int)x.Identifier, 
                    x => (IFactory<Vector3?, CreatureView>)ctx.Container.ResolveId<CreatureView.Factory>(x.Identifier));
        }

        private Dictionary<int, IFactory<string, int, bool, CollectableItem>> GetCollectableItemFactories(InjectContext ctx)
        {
            return ctx.Container.AllContracts.Where(
                x => x.Type == typeof(CollectableItem.Factory))
                .ToDictionary(x => (int)x.Identifier,
                    x => (IFactory<string, int, bool, CollectableItem>)ctx.Container.ResolveId<CollectableItem.Factory>(x.Identifier));
        }
    }

    public class CreatureViewFactory
    {
        readonly Dictionary<int, IFactory<Vector3?, CreatureView>> _subFactories;

        public CreatureViewFactory(
            Dictionary<int, IFactory<Vector3?, CreatureView>> subFactories)
        {
            _subFactories = subFactories;
        }

        public CreatureView Create(int key, Vector3? pos)
        {
            return _subFactories[key].Create(pos);
        }
    }

    public class CollectableItemFactory
    {
        readonly Dictionary<int, IFactory<string, int, bool, CollectableItem>> _subFactories;

        public CollectableItemFactory(
            Dictionary<int, IFactory<string, int, bool, CollectableItem>> subFactories)
        {
            _subFactories = subFactories;
        }

        public CollectableItem Create(int key, string creatureId, int level, bool isIdle)
        {
            return _subFactories[key].Create(creatureId, level, isIdle);
        }
    }
}